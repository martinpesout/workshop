Workshop "Základy dnešního vývoje frontendu"

Na čtyřech ukázkách si postupně ukážeme jak lze do projektu implementovat SASS
a jak lze automatizovat řadu dalších užitečných úkonů. Vše by mělo pomoci
frontendistům zaměřit se na užitečnou práci a neřešit zbytečnosti při psaní
svého kódu.