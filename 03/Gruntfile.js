'use strict';

module.exports = function(grunt) {

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');

	grunt.initConfig({



		sass: {                              // Task
			dist: {                            // Target
				options: {                       // Target options
					style: 'expanded'
				},
				files: {                         // Dictionary of files
					'css/styles.css': 'scss/styles.scss',       // 'destination': 'source'
				}
			}
		},



		autoprefixer: {
			options: {
				browsers: ['last 8 versions'],
			},
			dist: {
				files: [{
					src: [
						'css/styles.css',
					]
				}]
			}
		},



	});

	grunt.registerTask('default', ['sass', 'autoprefixer']);
};
